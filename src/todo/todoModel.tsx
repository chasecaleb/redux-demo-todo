export interface TodoModel {
    readonly id: number;
    readonly text: string;
    readonly completed: boolean;
}
