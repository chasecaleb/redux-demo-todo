import * as React from "react";
import { connect } from "react-redux";
import { RootState } from "../app/rootReducer";
import { TodoModel } from "./todoModel";
import { getTodos } from "./todoReducers";

interface TodoListProps {
    readonly todos: ReadonlyArray<TodoModel>;
}

export const TodoListView: React.SFC<TodoListProps> = props => {
    const elements = props.todos.map(it => <TodoItem model={it} key={it.id} />);
    return <ul>{elements}</ul>;
};

const TodoItem: React.SFC<{ readonly model: TodoModel }> = props => (
    <li style={{ textDecoration: props.model.completed ? "line-through" : "" }}>
        {props.model.text}
    </li>
);

const mapStateToProps = (state: RootState) => ({
    todos: getTodos(state)
});

export const TodoList = connect(mapStateToProps)(TodoListView);
