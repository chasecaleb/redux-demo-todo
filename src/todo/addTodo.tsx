import * as React from "react";
import { FormEvent } from "react";
import { connect, DispatchProp } from "react-redux";
import { addTodo } from "./todoActions";

interface AddTodoState {
    readonly text: string;
}

export class AddTodoView extends React.Component<DispatchProp, AddTodoState> {
    constructor(props: DispatchProp) {
        super(props);
        this.state = { text: "" };
    }

    // TODO: extract input component for text w/ validation (must not be empty)
    render(): React.ReactNode {
        return (
            <form onSubmit={this.handleSubmit}>
                <input
                    type="text"
                    value={this.state.text}
                    onChange={it => this.setState({ text: it.target.value })}
                />
                <button type="submit">Add</button>
            </form>
        );
    }

    readonly handleSubmit = (event: FormEvent<{}>) => {
        event.preventDefault();
        this.props.dispatch!(addTodo(this.state.text));
        this.setState({ text: "" });
    };
}

export const AddTodo = connect()(AddTodoView);
