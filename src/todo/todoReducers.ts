import { RootState } from "../app/rootReducer";
import { StandardReducer } from "../util/reduxTypes";
import { TodoActions } from "./todoActions";
import { TodoModel } from "./todoModel";

export interface TodoState {
    readonly items: ReadonlyArray<TodoModel>;
}

export const todoReducer: StandardReducer<TodoState, TodoActions> = (
    state = { items: [] },
    action
) => {
    // TODO: handle update/delete
    switch (action.type) {
        case "ADD_TODO":
            return {
                items: [...state.items, action.payload]
            };
        default:
            return state;
    }
};

export const getTodos = (state: RootState) => state.todo.items;
