import * as React from "react";
import { AddTodo } from "./addTodo";
import { TodoList } from "./todoList";

export const Todos: React.SFC = () => (
    <>
        <AddTodo />
        <TodoList />
    </>
);
