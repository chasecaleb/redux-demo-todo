import { StandardAction } from "../util/reduxTypes";
import { TodoModel } from "./todoModel";

export type AddTodoAction = StandardAction<"ADD_TODO", TodoModel>;
export type UpdateTodoAction = StandardAction<"UPDATE_TODO", TodoModel>;
export type DeleteTodoAction = StandardAction<
    "DELETE_TODO",
    { readonly id: number }
>;

// TODO: implement remaining actions.
export type TodoActions = AddTodoAction | UpdateTodoAction | DeleteTodoAction;

export const addTodo = (text: string): AddTodoAction => ({
    payload: {
        completed: false,
        // This is obviously a horrible way to generate "unique" IDs, but it works for a demo.
        id: new Date().getTime(),
        text
    },
    type: "ADD_TODO"
});
