import * as React from "react";

export const DisplayFilter: React.SFC = () => (
    <div>
        <span>Show:</span>
        <FilterButton name="All" disabled={true} />
        <FilterButton name="Active" disabled={true} />
        <FilterButton name="Completed" disabled={true} />
    </div>
);

interface FilterButtonProps {
    readonly name: string;
    readonly disabled: boolean;
}

export const FilterButton: React.SFC<FilterButtonProps> = props => (
    <button disabled={props.disabled}>{props.name}</button>
);
