/**
 * Flux standard action pattern: https://github.com/redux-utilities/flux-standard-action
 *
 * Note: meta field omitted since there isn't a use for it in this project at the moment.
 */
import { Action } from "redux";

export interface StandardAction<T extends string, P> extends Action {
    readonly type: T;
    readonly payload: P;
    readonly error?: boolean;
}

interface OtherAction extends Action {
    readonly type: "__ignored_other_action__";
}

export type StandardReducer<S, A extends StandardAction<any, any>> = (
    state: S | undefined,
    action: A | OtherAction
) => S;
