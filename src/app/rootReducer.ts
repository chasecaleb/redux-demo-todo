import { combineReducers } from "redux";
import { todoReducer, TodoState } from "../todo/todoReducers";

export interface RootState {
    readonly todo: TodoState;
}

export const rootReducer = combineReducers<RootState>({
    todo: todoReducer
});
