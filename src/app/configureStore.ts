// Enables https://github.com/zalmoxisus/redux-devtools-extension
import { createStore, Store } from "redux";
import { rootReducer, RootState } from "./rootReducer";

const devToolsEnhancer =
    (window as any).__REDUX_DEVTOOLS_EXTENSION__ &&
    (window as any).__REDUX_DEVTOOLS_EXTENSION__();
export const configureStore = (): Store<RootState> =>
    createStore(rootReducer, devToolsEnhancer);
