import * as React from "react";
import { DisplayFilter } from "../filter/displayFilter";
import { Todos } from "../todo/todos";

export const App: React.SFC = () => (
    <>
        <Todos />
        <DisplayFilter />
    </>
);
