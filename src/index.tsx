import * as React from "react";
import { render } from "react-dom";
import { Provider } from "react-redux";
import { App } from "./app/app";
import { configureStore } from "./app/configureStore";
import registerServiceWorker from "./registerServiceWorker";

const store = configureStore();
render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById("root") as HTMLElement
);
registerServiceWorker();
