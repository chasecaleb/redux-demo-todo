import * as React from "react";

import { action } from "@storybook/addon-actions";
import { boolean, text, withKnobs } from "@storybook/addon-knobs";
import { linkTo } from "@storybook/addon-links";
import { storiesOf } from "@storybook/react";
import { Provider } from "react-redux";
import { App } from "../src/app/app";
import { configureStore } from "../src/app/configureStore";
import { TodoListView } from "../src/todo/todoList";
import { TodoModel } from "../src/todo/todoModel";
import { Todos } from "../src/todo/todos";

const reduxStore = configureStore();
storiesOf("App", module)
    .addDecorator(render => <Provider store={reduxStore}>{render()}</Provider>)
    .add("demo", () => <App />);

storiesOf("Todos", module)
    .addDecorator(withKnobs)
    .add("list view", () => {
        const items: ReadonlyArray<TodoModel> = [
            { id: 1, completed: true, text: "setup storybook" },
            { id: 2, completed: false, text: "unfinished business" },
            {
                completed: boolean("Completed", false),
                id: 2,
                text: text("Text", "check out the 'knobs' tab to change me")
            }
        ];
        return <TodoListView todos={items} />;
    });
