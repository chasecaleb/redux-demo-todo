import * as original from "react-redux";
import {Action} from "redux";

declare module "react-redux" {
    /*
     * Add default for irrelevant generic type.
     * Original definition doesn't do this because generic defaults are a
     * relatively new Typescript feature, and the maintainer didn't want to
     * break backwards compatibility.
     */
    export interface DispatchProp<S = Action> {
        // tslint:disable-next-line
        dispatch?: original.Dispatch<S>;
    }
}
